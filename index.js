const app = require('./app');
const database = require('./db');
const config = require('./config');

database().then(() => {
    console.log(`Connected to mongodb://localhost:17027/blog`);
    app.listen(config.PORT, () => 
    console.log(`MEANwhile app listening on port ${config.PORT} !`)
    );
}).catch(() => {
    throw new Error("Error connecting to database");
});